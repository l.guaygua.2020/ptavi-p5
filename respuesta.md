## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura? 
## 1050 Paquetes
* ¿Cuánto tiempo dura la captura? 
## Dura 10.521629838 segundos
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?
## La dirección IP es: 192.168.1.116
* ¿Se trata de una IP pública o de una IP privada?
## IP privada
* ¿Por qué lo sabes?   
## Porque su dirección ip pertenece a la clase C y se encuentra en el rango de ip privadas  

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
## Son SIP, RTP y UDP
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
## Se puede ver ethernet II, IPv4 y ICMP
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
## RTP es el protocolo que presenta más tráfico con 137k bits/s
Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo corresponde con una llamada SIP.

Filtra por `sip` para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

* ¿En qué segundos tienen lugar los dos primeros envíos SIP?
## en 0.000012938 y 0.63305950
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
## En la trama 6
* Los paquetes RTP, ¿cada cuánto se envían?
## Cada 0.01s 

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

* ¿De qué protocolo de nivel de aplicación son?
## Son del protocolo SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?
## Dirección IP de la máquina "Linphone": 192.168.1.116
* ¿Cuál es la dirección IP de la máquina "Servidor"?
## dirección IP de la máquina "Servidor": 212.79.111.155
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
## linphone envía un request: INVITE al servidor
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
## En la segunda trama el servidor contesta con un 100 trying a linphone
Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
##  Del protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
## Entre el servidor(212.79.111.155) y linphone(192.168.1.116)
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
## El servidor manda su estado: 200 OK, quiere decir que la conexion está completada
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
## linphone manda un ACK al servidor.

## Ejercicio 5. Tramas finales

Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?
## La trama 1042
* ¿De qué máquina a qué máquina va?
## Va de linphone(192.168.1.116) al servidor(212.79.111.155)
* ¿Para qué sirve?
## el cliente envía un BYE, para terminar la comunicación
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
## User-Agent: Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37

## Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
##  se quiere establecer una llamada con music@sip.iptel.org
* ¿Qué instrucciones SIP entiende el UA?
## Entiende las instrucciones: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE
* ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
## la cabecera Content-Type: application/sdp
* ¿Cuál es el nombre de la sesión SIP?
## Session Name (s): Talk


## Ejercicio 7. Indicación de comienzo de conversación

En la propuesta SDP de Linphone puede verse un campo `m` con un valor que empieza por `audio 7078`.

* ¿Qué trama lleva esta propuesta?
## La segunda trama
* ¿Qué indica el `7078`?
## Indica el puerto
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## Que se enviarán a ese puerto(7078)
* ¿Qué paquetes son esos?
## son paquetes RTP 
En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.

* ¿Qué trama lleva esta respuesta?
## La trama 4
* ¿Qué valor es el `XXX`?
## 29448
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## será el puerto por donde se transmite. 
* ¿Qué paquetes son esos?
## son paquetes RTP

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?
## Va de 192.168.1.116 a 212.79.111.155
* ¿Qué tipo de datos transporta?
## Transporta datos multimedia
* ¿Qué tamaño tiene?
## 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
## Van 172 bytes
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
## cada 0.01 s

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
## Hay dos flujos, linphone-servidor y servidor-linphone 
* ¿Cuántos paquetes se pierden?
## no se pierden paquetes
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
## el valor máximo del delta es 30.726700
* ¿Qué es lo que significa el valor de delta?
## Lo que se tarda el envío entre paquetes
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
## En el del servidor a linphone
* ¿Qué significan esos valores?
## Al retardo entre paquetes

Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción `Telephony`, `RTP`, `RTP Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
## delta vale 0.000164 y el jitter vale 3.087182
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
##  
* El "skew" es negativo, ¿qué quiere decir eso?
## Que el paquete llegará con retraso

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
* ## No se escucha nada
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
* ¿A qué se debe la diferencia?


